<?php namespace App;

class Project
{
    public static function get($project_id, $year = null, $month = null)
    {
        $project_id = addslashes($project_id);
        $rows = get_all("SELECT
                        grproject.CODE as projectCode,
                        grproject.ID as projectId,
                        grproject.NAME as projectName,
                        grproject.U_work_days as workDays,
                        concat(foreman.FIRSTNAME, ' ', foreman.SURNAME) as foremanName,
                        grcomment.IDEREMPLOYEE as foremanId,
                        concat(employee.FIRSTNAME, ' ', employee.SURNAME) as employeeName,
                        employee.CODE as employeeCode,
                        employee.ID as employeeId,
                        DATE_FORMAT(grcomment.u_algus, '%Y-%m-%d') as foremanResponsibilityStartDate,
                        IF(grcomment.u_lopp = '1899-12-30 00:00:00','9999-12-30' , DATE_FORMAT(grcomment.u_lopp,'%Y-%m-%d')) as foremanResponsibilityEndDate
                    FROM grproject
                        LEFT JOIN grcomment on grproject.ID = grcomment.IDGRPROJECT
                        LEFT JOIN eremployee as foreman on grcomment.IDEREMPLOYEE = foreman.ID
                        LEFT JOIN eremployee as employee on grproject.U_default_employee = employee.ID
                    WHERE grproject.ID = $project_id
        ");
        if (empty($rows)) {
            return [];
        }
        $result = [];
        $result['salaryFunds'] = Salary::getProjectSalaries($project_id, $month, $year);
        $now = date('Y-m-d');
        foreach ($rows as $row) {
            $result['projectId'] = $row['projectId'];
            $result['projectCode'] = $row['projectCode'];
            $result['projectName'] = $row['projectName'];
            $result['workDays'] = $row['workDays'];

            // Current foreman
            if ($now >= $row['foremanResponsibilityStartDate'] && $now <= $row['foremanResponsibilityEndDate']) {
                $result['currentForeman'] = [
                    'foremanName' => $row['foremanName'],
                    'foremanId' => $row['foremanId'],
                    'foremanResponsibilityEndDate' => $row['foremanResponsibilityEndDate'],
                    'foremanResponsibilityStartDate' => $row['foremanResponsibilityStartDate']
                ];
            }
            $result['employeeName'] = $row['employeeName'];
            $result['employeeId'] = $row['employeeId'];
            $result['employeeCode'] = $row['employeeCode'];
            if (!$row['foremanResponsibilityStartDate']) {
                continue;
            }
            $result['foremen'][] = [
                'foremanName' => $row['foremanName'],
                'foremanId' => $row['foremanId'],
                'foremanResponsibilityEndDate' => $row['foremanResponsibilityEndDate'],
                'foremanResponsibilityStartDate' => $row['foremanResponsibilityStartDate']
            ];

        }
        $result['workDays'] = $result['workDays'] !== null && $result['workDays'] !== '' ? json_decode($result['workDays']) : [];
        return $result;
    }

    public static function getEmployeeProjects($id, $year)
    {
        if ($id === "0") {
            $user = "";
        } else {
            $user = "AND u.user_id = $id";
        }
        return get_all("SELECT
                                p.id as projectId,
                                concat(e.FIRSTNAME, ' ', e.SURNAME) as foremanName,
                                p.name as projectName,
                                sum(m1 + m2 + m3 + m4 + m5 + m6 + m7 + m8 + m9 + m10 + m11 + m12) as profit
                            FROM bbookingrow_cache c
                                LEFT JOIN eremployee e on e.id = c.IDWORKER
                                LEFT join grproject p on p.id = c.IDGRPROJECT
                                LEFT JOIN users u on u.employee_id = e.id
                            WHERE YEAR = $year $user
                            GROUP BY foremanName, projectName");
    }

    public static function getProjectBalance($project_id, $year, $user_id = false)
    {

        $user_filter = $user_id ? " AND u.user_id=$user_id " : "";
        return get_all("SELECT DISTINCT 
                                    p.name as projectName,
                                    c.PROJECT_CODE as projectCode,
                                    c.YEAR as year,
                                    c.ACCOUNT_NAME as accountName,
                                    c.ACCOUNT_CODE as accountCode,
                                    if (c.U_LIIK = 'atulu','revenue',REPLACE(c.U_LIIK, 'kulu', 'expense') ) as accountType,
                                    c.M1, c.M2, c.M3, c.M4, c.M5, c.M6, c.M7, c.M8, c.M9, c.M10, c.M11, c.M12,
                                    c.C1, c.C2, c.C3, c.C4, c.C5, c.C6, c.C7, c.C8, c.C9, c.C10, c.C11, c.C12
                                FROM bbookingrow_cache c
                                    LEFT JOIN eremployee e ON e.id = c.IDWORKER 
                                    LEFT JOIN grproject p ON p.id = c.IDGRPROJECT
                                    LEFT JOIN users u ON u.employee_id = c.IDWORKER 
                                WHERE YEAR = $year 
                                      AND p.id = $project_id" . $user_filter . "
                                ORDER BY U_LIIK ASC");

    }

    /**
     * @param $projectBalanceItem
     * @param $multiplier
     * @return array
     */
    public static function createAccountObject($projectBalanceItem, $multiplier): array
    {
        $temp_account = [];
        $temp_account["accountName"] = $projectBalanceItem["accountName"];
        $temp_account["accountCode"] = $projectBalanceItem["accountCode"];
        $temp_account["accountMonthSums"] = [
            "m1" => $multiplier * abs($projectBalanceItem["M1"]),
            "m2" => $multiplier * abs($projectBalanceItem["M2"]),
            "m3" => $multiplier * abs($projectBalanceItem["M3"]),
            "m4" => $multiplier * abs($projectBalanceItem["M4"]),
            "m5" => $multiplier * abs($projectBalanceItem["M5"]),
            "m6" => $multiplier * abs($projectBalanceItem["M6"]),
            "m7" => $multiplier * abs($projectBalanceItem["M7"]),
            "m8" => $multiplier * abs($projectBalanceItem["M8"]),
            "m9" => $multiplier * abs($projectBalanceItem["M9"]),
            "m10" => $multiplier * abs($projectBalanceItem["M10"]),
            "m11" => $multiplier * abs($projectBalanceItem["M11"]),
            "m12" => $multiplier * abs($projectBalanceItem["M12"]),
        ];

        $temp_account["transactionCounts"] = [
            "c1" => $projectBalanceItem["C1"],
            "c2" => $projectBalanceItem["C2"],
            "c3" => $projectBalanceItem["C3"],
            "c4" => $projectBalanceItem["C4"],
            "c5" => $projectBalanceItem["C5"],
            "c6" => $projectBalanceItem["C6"],
            "c7" => $projectBalanceItem["C7"],
            "c8" => $projectBalanceItem["C8"],
            "c9" => $projectBalanceItem["C9"],
            "c10" => $projectBalanceItem["C10"],
            "c11" => $projectBalanceItem["C11"],
            "c12" => $projectBalanceItem["C12"],
        ];
        $temp_account["totalSum"] = $multiplier * (
                abs($projectBalanceItem["M1"]) +
                abs($projectBalanceItem["M2"]) +
                abs($projectBalanceItem["M3"]) +
                abs($projectBalanceItem["M4"]) +
                abs($projectBalanceItem["M5"]) +
                abs($projectBalanceItem["M6"]) +
                abs($projectBalanceItem["M7"]) +
                abs($projectBalanceItem["M8"]) +
                abs($projectBalanceItem["M9"]) +
                abs($projectBalanceItem["M10"]) +
                abs($projectBalanceItem["M11"]) +
                abs($projectBalanceItem["M12"]));
        return $temp_account;
    }

    /**
     * @param array $type_object
     * @param $type
     * @return mixed
     */
    public static function calculateSumForAccountBasedOnType(array $type_object, $type)
    {
        return $type_object[$type]["accountMonthSums"]["m1"] +
            $type_object[$type]["accountMonthSums"]["m2"] +
            $type_object[$type]["accountMonthSums"]["m3"] +
            $type_object[$type]["accountMonthSums"]["m4"] +
            $type_object[$type]["accountMonthSums"]["m5"] +
            $type_object[$type]["accountMonthSums"]["m6"] +
            $type_object[$type]["accountMonthSums"]["m7"] +
            $type_object[$type]["accountMonthSums"]["m8"] +
            $type_object[$type]["accountMonthSums"]["m9"] +
            $type_object[$type]["accountMonthSums"]["m10"] +
            $type_object[$type]["accountMonthSums"]["m11"] +
            $type_object[$type]["accountMonthSums"]["m12"];
    }

    /**
     * @param array $accountGroups
     * @return array $margins
     */
    public static function getMarginsAndExpenses(array $accountGroups): array
    {
        $margins = [];
        $sum = [
            "m1" => 0.0,
            "m2" => 0.0,
            "m3" => 0.0,
            "m4" => 0.0,
            "m5" => 0.0,
            "m6" => 0.0,
            "m7" => 0.0,
            "m8" => 0.0,
            "m9" => 0.0,
            "m10" => 0.0,
            "m11" => 0.0,
            "m12" => 0.0,
        ];
        $expenseSums = [];
        for ($i = 1; $i < 7; $i++) {
            $accountGroupType = "expense" . $i;
            if (!key_exists($accountGroupType, $margins) && key_exists($accountGroupType, $accountGroups)) {
                $margins[$accountGroupType] = [];
            }
            if (key_exists($accountGroupType, $accountGroups)) {
                $sum = [
                    "m1" => $sum["m1"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m1"],
                    "m2" => $sum["m2"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m2"],
                    "m3" => $sum["m3"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m3"],
                    "m4" => $sum["m4"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m4"],
                    "m5" => $sum["m5"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m5"],
                    "m6" => $sum["m6"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m6"],
                    "m7" => $sum["m7"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m7"],
                    "m8" => $sum["m8"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m8"],
                    "m9" => $sum["m9"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m9"],
                    "m10" => $sum["m10"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m10"],
                    "m11" => $sum["m11"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m11"],
                    "m12" => $sum["m12"] + $accountGroups[$accountGroupType]["accountMonthSums"]["m12"],
                ];

                if (!key_exists("accountMonthSums", $margins[$accountGroupType])) {
                    $margins[$accountGroupType]["accountMonthSums"] = [];
                    $margins[$accountGroupType]["accountMonthSums"] = [
                        "m1" => $sum["m1"],
                        "m2" => $sum["m2"],
                        "m3" => $sum["m3"],
                        "m4" => $sum["m4"],
                        "m5" => $sum["m5"],
                        "m6" => $sum["m6"],
                        "m7" => $sum["m7"],
                        "m8" => $sum["m8"],
                        "m9" => $sum["m9"],
                        "m10" => $sum["m10"],
                        "m11" => $sum["m11"],
                        "m12" => $sum["m12"],

                    ];
                }


                if (key_exists("revenue", $accountGroups)) {
                    $margins[$accountGroupType]["accountMonthSums"]["m1"] += $accountGroups["revenue"]["accountMonthSums"]["m1"];
                    $margins[$accountGroupType]["accountMonthSums"]["m2"] += $accountGroups["revenue"]["accountMonthSums"]["m2"];
                    $margins[$accountGroupType]["accountMonthSums"]["m3"] += $accountGroups["revenue"]["accountMonthSums"]["m3"];
                    $margins[$accountGroupType]["accountMonthSums"]["m4"] += $accountGroups["revenue"]["accountMonthSums"]["m4"];
                    $margins[$accountGroupType]["accountMonthSums"]["m5"] += $accountGroups["revenue"]["accountMonthSums"]["m5"];
                    $margins[$accountGroupType]["accountMonthSums"]["m6"] += $accountGroups["revenue"]["accountMonthSums"]["m6"];
                    $margins[$accountGroupType]["accountMonthSums"]["m7"] += $accountGroups["revenue"]["accountMonthSums"]["m7"];
                    $margins[$accountGroupType]["accountMonthSums"]["m8"] += $accountGroups["revenue"]["accountMonthSums"]["m8"];
                    $margins[$accountGroupType]["accountMonthSums"]["m9"] += $accountGroups["revenue"]["accountMonthSums"]["m9"];
                    $margins[$accountGroupType]["accountMonthSums"]["m10"] += $accountGroups["revenue"]["accountMonthSums"]["m10"];
                    $margins[$accountGroupType]["accountMonthSums"]["m11"] += $accountGroups["revenue"]["accountMonthSums"]["m11"];
                    $margins[$accountGroupType]["accountMonthSums"]["m12"] += $accountGroups["revenue"]["accountMonthSums"]["m12"];
                }
                $margins[$accountGroupType]["totalSum"] = self::calculateSumForAccountBasedOnType($margins, $accountGroupType);
            }
        }
        $expenseSums["accountMonthSums"] = $sum;
        $expenseSums["totalSum"] =
            $sum["m1"] +
            $sum["m2"] +
            $sum["m3"] +
            $sum["m4"] +
            $sum["m5"] +
            $sum["m6"] +
            $sum["m7"] +
            $sum["m8"] +
            $sum["m9"] +
            $sum["m10"] +
            $sum["m11"] +
            $sum["m12"];
        return array($margins, $expenseSums);
    }

    /**
     * @param array $project
     * @return array $totals
     */
    public static function getTotals(array $project): array
    {
        $totals = [];
        $accountMonthSums = [
            "m1" => 0.0,
            "m2" => 0.0,
            "m3" => 0.0,
            "m4" => 0.0,
            "m5" => 0.0,
            "m6" => 0.0,
            "m7" => 0.0,
            "m8" => 0.0,
            "m9" => 0.0,
            "m10" => 0.0,
            "m11" => 0.0,
            "m12" => 0.0,
        ];
        if (key_exists("revenue", $project["accountGroups"])) {
            $accountMonthSums = $project["accountGroups"]["revenue"]["accountMonthSums"];
        }
        $expenses_sum = $accountMonthSums;
        if (key_exists("expenses", $project)) {
            $expenses_sum = $project["expenses"]["accountMonthSums"];
        }
        $totals["accountMonthSums"] = [
            "m1" => $accountMonthSums["m1"] + $expenses_sum["m1"],
            "m2" => $accountMonthSums["m2"] + $expenses_sum["m2"],
            "m3" => $accountMonthSums["m3"] + $expenses_sum["m3"],
            "m4" => $accountMonthSums["m4"] + $expenses_sum["m4"],
            "m5" => $accountMonthSums["m5"] + $expenses_sum["m5"],
            "m6" => $accountMonthSums["m6"] + $expenses_sum["m6"],
            "m7" => $accountMonthSums["m7"] + $expenses_sum["m7"],
            "m8" => $accountMonthSums["m8"] + $expenses_sum["m8"],
            "m9" => $accountMonthSums["m9"] + $expenses_sum["m9"],
            "m10" => $accountMonthSums["m10"] + $expenses_sum["m10"],
            "m11" => $accountMonthSums["m11"] + $expenses_sum["m11"],
            "m12" => $accountMonthSums["m12"] + $expenses_sum["m12"]
        ];
        $totals["totalSum"] =
            $totals["accountMonthSums"]["m1"] +
            $totals["accountMonthSums"]["m2"] +
            $totals["accountMonthSums"]["m3"] +
            $totals["accountMonthSums"]["m4"] +
            $totals["accountMonthSums"]["m5"] +
            $totals["accountMonthSums"]["m6"] +
            $totals["accountMonthSums"]["m7"] +
            $totals["accountMonthSums"]["m8"] +
            $totals["accountMonthSums"]["m9"] +
            $totals["accountMonthSums"]["m10"] +
            $totals["accountMonthSums"]["m11"] +
            $totals["accountMonthSums"]["m12"];

        return $totals;
    }

    public static function getProjectTransactions($project_id, $account_code, $year, $month, $user_id = false)
    {
        $user_filter = $user_id ? " AND u.user_id=$user_id " : "";
        $month_filter = $month ? " AND MONTH(B.DEALDATE) = $month" : "";
        $year_filter = $year ? " AND YEAR(B.DEALDATE) = $year" : "";

        return get_all("SELECT *
                            FROM (SELECT 
                                         gdocument.DOCNO                             AS documentNumber,
                                         gdocument.SOURCEDOCNO                       AS sourceDocumentNumber,
                                         DATE(B.DOCDATE)                             AS documentDate,
                                         DATE(B.DEALDATE)                            AS transactionDate,
                                         IFnull(grcompany.NAME, '')                  AS companyName,
                                         CONCAT(e_tj.FIRSTNAME, ' ', e_tj.SURNAME)   AS foremanName,
                                         braccount.u_liik                            AS accountType,
                                         bbookingrow.AMOUNTBASE                      AS amountBase,
                                         gdocument.TOTALAMOUNT                       AS totalAmount,
                                         braccount.CODE                              AS accountCode,
                                         braccount.NAME                              AS accountName,
                                         IF(c.IDEREMPLOYEE = e_tj.id, 1, 0)            AS isOwner,
                                         IF(c.u_algus IS NULL, 0, IF(SUM(IF(B.DEALDATE BETWEEN c.u_algus AND c.u_lopp, 1, 0)) > 0, 1, 0))
                                                                                     AS isOK
                                  FROM bbooking AS B
                                           RIGHT JOIN gdocument ON (gdocument.ID = B.IDDOC)
                                           LEFT JOIN bbookingrow ON (B.ID = bbookingrow.IDBBooking)
                                           LEFT JOIN braccount ON (bbookingrow.IDC_Account = braccount.ID)
                                           LEFT JOIN grcompany ON (grcompany.ID = gdocument.IDGRCOMPANY)
                                           LEFT JOIN eremployee AS e_tj ON (bbookingrow.IDWORKER = e_tj.ID)
                                           LEFT JOIN eremployee AS e_pt ON (B.IDEREMPLOYEE = e_pt.ID)
                                           LEFT JOIN eremployee AS e_rmp ON (gdocument.IDEREMPLOYEE = e_rmp.ID)
                                           LEFT JOIN grproject AS P ON (P.id = bbookingrow.idc_grproject)
                                           LEFT JOIN (SELECT IDGRPROJECT,
                                                             IDEREMPLOYEE,
                                                             u_algus,
                                                             IF(u_lopp < '1999-12-31 00:00:00', '2037-12-31 00:00:00',
                                                                u_lopp) AS u_lopp
                                                      FROM grcomment) AS c
                                                     ON (bbookingrow.IDWORKER = c.IDEREMPLOYEE AND P.ID = c.IDGRPROJECT)
                                  WHERE 1
                                    AND B.SOURCEDOCNO NOT LIKE '%Aasta sulgemine%'
                                    AND braccount.CODE = '$account_code'
                                    AND bbookingrow.IDC_GRPROJECT = $project_id
                                    " . $month_filter . "
                                    " . $year_filter . "
                                  GROUP BY bbookingrow.ID,
                                           P.name,
                                           documentNumber,
                                           sourceDocumentNumber,
                                           grcompany.NAME,
                                           e_tj.FIRSTNAME,
                                           e_tj.SURNAME,
                                           e_tj.CODE,
                                           e_pt.FIRSTNAME,
                                           e_pt.SURNAME,
                                           e_rmp.FIRSTNAME,
                                           e_rmp.SURNAME,
                                           totalAmount,
                                           c.u_algus,
                                           accountType,
                                           accountCode,
                                           accountName
                                  UNION ALL
                            
                                  SELECT 
                                         gdocument.DOCNO                             AS documentNumber,
                                         gdocument.SOURCEDOCNO                       AS sourceDocumentNumber,
                                         DATE(B.DOCDATE)                             AS documentDate,
                                         DATE(B.DEALDATE)                            AS transactionDate,
                                         IFnull(grcompany.NAME, '')                  AS companyName,
                                         CONCAT(e_tj.FIRSTNAME, ' ', e_tj.SURNAME)   AS foremanName,
                                         braccount.u_liik                            AS accountType,
                                         bbookingrow.AMOUNTBASE * -1                 AS amountBase,
                                         gdocument.TOTALAMOUNT                       AS totalAmount,
                                         braccount.CODE                              AS accountCode,
                                         braccount.NAME                              AS accountName,
                                         IF(c.IDEREMPLOYEE = e_tj.id, 1, 0)            AS isOwner,
                                         IF(c.u_algus IS NULL, 0, IF(SUM(IF(B.DEALDATE BETWEEN c.u_algus AND c.u_lopp, 1, 0)) > 0, 1, 0))
                                                                                     AS isOK
                                  FROM bbooking AS B
                                           RIGHT JOIN gdocument ON (gdocument.ID = B.IDDOC)
                                           LEFT JOIN bbookingrow ON (B.ID = bbookingrow.IDBBooking)
                                           LEFT JOIN braccount ON (bbookingrow.IDD_Account = braccount.ID)
                                           LEFT JOIN grcompany ON (grcompany.ID = gdocument.IDGRCOMPANY)
                                           LEFT JOIN eremployee AS e_tj ON (bbookingrow.IDWORKER = e_tj.ID)
                                           LEFT JOIN eremployee AS e_pt ON (B.IDEREMPLOYEE = e_pt.ID)
                                           LEFT JOIN eremployee AS e_rmp ON (gdocument.IDEREMPLOYEE = e_rmp.ID)
                                           LEFT JOIN grproject AS P ON (P.id = bbookingrow.idd_grproject)
                                           LEFT JOIN (SELECT IDGRPROJECT,
                                                             IDEREMPLOYEE,
                                                             u_algus,
                                                             IF(u_lopp < '1999-12-31 00:00:00', '2037-12-31 00:00:00',
                                                                u_lopp) AS u_lopp
                                                      FROM grcomment) AS c
                                                     ON (bbookingrow.IDWORKER = c.IDEREMPLOYEE AND P.ID = c.IDGRPROJECT)
                                  WHERE 1
                                    AND B.SOURCEDOCNO NOT LIKE '%Aasta sulgemine%'
                                    AND braccount.CODE = '$account_code'
                                    AND bbookingrow.IDD_GRPROJECT = $project_id
                                    " . $month_filter . "
                                    " . $year_filter . "
                                  GROUP BY bbookingrow.ID,
                                           P.name,
                                           e_tj.FIRSTNAME,
                                           e_rmp.FIRSTNAME,
                                           e_pt.SURNAME,
                                           e_rmp.SURNAME,
                                           documentNumber,
                                           sourceDocumentNumber,
                                           grcompany.NAME,
                                           e_tj.FIRSTNAME,
                                           e_tj.SURNAME,
                                           e_tj.CODE,
                                           e_pt.FIRSTNAME,
                                           e_pt.SURNAME,
                                           e_rmp.FIRSTNAME,
                                           e_rmp.SURNAME,
                                           totalAmount,
                                           c.u_algus,
                                           accountType,
                                           accountCode,
                                           accountName
                                 ) AS foo
                            WHERE accountType != ''
                            ORDER BY foremanName, transactionDate
                            ");
    }

    public static function setDefaultEmployee(int $projectId, int $employeeId)
    {
        update('grproject', ['U_default_employee' => $employeeId], "ID = $projectId");
    }
}