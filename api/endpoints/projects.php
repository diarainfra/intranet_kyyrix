<?php

use App\Project;


/**
 * Returns a list of projects which correspond to given year for logged in user
 * @example GET /projects
 * @example GET /projects?year=2019
 * @returns { "projects": {...} }
 */
$app->get('#^projects\/?$#', function ($params, $requestBody) {

    // Default year is current year
    $year = empty($_GET['year']) ? date('Y') : $_GET['year'];
    // Make sure we have a valid year
    if (intval($year) <= 0) {
        stop(400, ["error" => "Invalid year"]);
    }

    // Return the projects for given year
    stop(200, ["projects" => Project::getEmployeeProjects($_GET['user'], $year)]);
});

/**
 * Returns given project details
 * @example GET /projects/123
 * @returns {"project": {...}"
 */
$app->get('#^projects/(?<id>\d+)\/?$#', function ($params) {

    $user_id = $_GET['user'];
    $project_id = (int)$params["id"];
    $year = isset($_GET['year']) ? (int)$_GET['year'] : date('Y');

    $project = [];
    if (!empty($this->user["isAdmin"]) && !isset($_GET['user'])) {
        // get complete project values
        $projectBalance_raw = Project::getProjectBalance($project_id, $year);
    } else {
        // get specific user's projects values
        $projectBalance_raw = Project::getProjectBalance($project_id, $year, $user_id);
    }


    if (!empty($projectBalance_raw)) {
        $project = [
            "projectName"           => $projectBalance_raw[0]["projectName"],
            "projectCode"           => $projectBalance_raw[0]["projectCode"],
            "year"           => $projectBalance_raw[0]["year"],
            "accountGroups" => []
        ];
        $temp_account_groups = [];
        foreach ($projectBalance_raw as $projectBalanceItem) {
            $accountType = $projectBalanceItem["accountType"];
            $multiplier = preg_match("/expense/", $accountType) ? -1 : 1;

            if (!key_exists($accountType, $temp_account_groups)) {
                $temp_account_groups[$accountType] = [];
            }

            $temp_account_groups[$accountType]["accountGroupName"] = $accountType;

            if (!key_exists("accounts", $temp_account_groups[$accountType])) {
                $temp_account_groups[$accountType]["accounts"] = [];
            }

            $temp_account_groups[$accountType]["accounts"][] = Project::createAccountObject($projectBalanceItem, $multiplier);

            if (!key_exists("accountMonthSums", $temp_account_groups[$accountType])) {
                $temp_account_groups[$accountType]["accountMonthSums"] = [
                    "m1"  => 0.0,
                    "m2"  => 0.0,
                    "m3"  => 0.0,
                    "m4"  => 0.0,
                    "m5"  => 0.0,
                    "m6"  => 0.0,
                    "m7"  => 0.0,
                    "m8"  => 0.0,
                    "m9"  => 0.0,
                    "m10" => 0.0,
                    "m11" => 0.0,
                    "m12" => 0.0,
                ];
            }
            $temp_account_groups[$accountType]["accountMonthSums"] = [
                "m1"  => $temp_account_groups[$accountType]["accountMonthSums"]["m1"] + $multiplier * abs($projectBalanceItem["M1"]),
                "m2"  => $temp_account_groups[$accountType]["accountMonthSums"]["m2"] + $multiplier * abs($projectBalanceItem["M2"]),
                "m3"  => $temp_account_groups[$accountType]["accountMonthSums"]["m3"] + $multiplier * abs($projectBalanceItem["M3"]),
                "m4"  => $temp_account_groups[$accountType]["accountMonthSums"]["m4"] + $multiplier * abs($projectBalanceItem["M4"]),
                "m5"  => $temp_account_groups[$accountType]["accountMonthSums"]["m5"] + $multiplier * abs($projectBalanceItem["M5"]),
                "m6"  => $temp_account_groups[$accountType]["accountMonthSums"]["m6"] + $multiplier * abs($projectBalanceItem["M6"]),
                "m7"  => $temp_account_groups[$accountType]["accountMonthSums"]["m7"] + $multiplier * abs($projectBalanceItem["M7"]),
                "m8"  => $temp_account_groups[$accountType]["accountMonthSums"]["m8"] + $multiplier * abs($projectBalanceItem["M8"]),
                "m9"  => $temp_account_groups[$accountType]["accountMonthSums"]["m9"] + $multiplier * abs($projectBalanceItem["M9"]),
                "m10" => $temp_account_groups[$accountType]["accountMonthSums"]["m10"] + $multiplier * abs($projectBalanceItem["M10"]),
                "m11" => $temp_account_groups[$accountType]["accountMonthSums"]["m11"] + $multiplier * abs($projectBalanceItem["M11"]),
                "m12" => $temp_account_groups[$accountType]["accountMonthSums"]["m12"] + $multiplier * abs($projectBalanceItem["M12"]),
            ];
            if (!key_exists("transactionCounts", $temp_account_groups[$accountType])) {
                $temp_account_groups[$accountType]["transactionCounts"] = [
                    "c1"  => 0,
                    "c2"  => 0,
                    "c3"  => 0,
                    "c4"  => 0,
                    "c5"  => 0,
                    "c6"  => 0,
                    "c7"  => 0,
                    "c8"  => 0,
                    "c9"  => 0,
                    "c10" => 0,
                    "c11" => 0,
                    "c12" => 0.,
                ];
            }
            $temp_account_groups[$accountType]["transactionCounts"] = [
                "c1"  => $temp_account_groups[$accountType]["transactionCounts"]["c1"] + $projectBalanceItem["C1"],
                "c2"  => $temp_account_groups[$accountType]["transactionCounts"]["c2"] + $projectBalanceItem["C2"],
                "c3"  => $temp_account_groups[$accountType]["transactionCounts"]["c3"] + $projectBalanceItem["C3"],
                "c4"  => $temp_account_groups[$accountType]["transactionCounts"]["c4"] + $projectBalanceItem["C4"],
                "c5"  => $temp_account_groups[$accountType]["transactionCounts"]["c5"] + $projectBalanceItem["C5"],
                "c6"  => $temp_account_groups[$accountType]["transactionCounts"]["c6"] + $projectBalanceItem["C6"],
                "c7"  => $temp_account_groups[$accountType]["transactionCounts"]["c7"] + $projectBalanceItem["C7"],
                "c8"  => $temp_account_groups[$accountType]["transactionCounts"]["c8"] + $projectBalanceItem["C8"],
                "c9"  => $temp_account_groups[$accountType]["transactionCounts"]["c9"] + $projectBalanceItem["C9"],
                "c10" => $temp_account_groups[$accountType]["transactionCounts"]["c10"] + $projectBalanceItem["C10"],
                "c11" => $temp_account_groups[$accountType]["transactionCounts"]["c11"] + $projectBalanceItem["C11"],
                "c12" => $temp_account_groups[$accountType]["transactionCounts"]["c12"] + $projectBalanceItem["C12"],
            ];

            $temp_account_groups[$accountType]["totalSum"] = Project::calculateSumForAccountBasedOnType($temp_account_groups, $accountType);

            $project["accountGroups"] = $temp_account_groups;
        }

        if (!empty($project["accountGroups"])) {
            list($project["margins"], $project["expenses"]) = Project::getMarginsAndExpenses($project["accountGroups"]);
            $project["totals"] = Project::getTotals($project);
        }


        // Return the project
        stop(200, ["project" => $project]);
    }
});

/**
 * Returns transactions for specified project and account
 * @example GET /projects/123/transactions/321
 * @returns {"transactions": {...}"
 */
$app->get('#^projects/(?<id>\d+)/transactions/(?<acc_code>\d+)\/?$#', function ($params) {
    $project_id = isset($params["id"]) ? $params["id"] : null;
    $account_code = isset($params["acc_code"]) ? $params["acc_code"] : null;
    $year = isset($_GET["year"]) ? $_GET["year"] : null;
    $month = isset($_GET["month"]) ? $_GET["month"] : null;
    if (!$project_id) {
        stop(403, ["Missing project id"]);
    }
    if (!$account_code) {
        stop(403, ["Missing account code"]);
    }
    $transactions = Project::getProjectTransactions($project_id, $account_code, $year, $month);
    $amount_base_sum = 0.0;
    $total_amount_sum = 0.0;
    foreach ($transactions as $transaction) {
        $amount_base_sum += $transaction["amountBase"];
        $total_amount_sum += $transaction["totalAmount"];
    }
    stop(200, [
        "transactions" => $transactions,
        "project"      => Project::get($project_id),
        "totalSums"         => ["amountBase" => $amount_base_sum, "totalAmount" => $total_amount_sum]
    ]);
});