<?php

use App\Holiday;
use App\User;
use App\Session;


/**
 * Creates a new session token (login)
 * @example:
 * POST /sessions
 *
 * {"username": "foo", "password": "bar"}
 *
 * @returns 400 Bad request
 * @returns 401 Unauthorized
 * @returns 200 OK {"user": {..., "token": "xxx", ...} }
 */
$app->post('#^sessions$#', function ($params, $requestBody) {

    // Make sure both username and password are provided
    if (empty($requestBody->username) || empty($requestBody->password)) {
        stop(400, ['error' => 'Missing username or password']);
    }

    // Make sure they're valid
    if (!$user = User::get([
        "username" =>$requestBody->username,
        "password" => $requestBody->password])
    ) {
        stop(401, ['error' => 'Invalid username or password']);
    }

    // Create new session
    $session = Session::create($user);
    // Check if it's time to refresh list of holidays
    if (!Holiday::synced()){
        Holiday::sync();
    }
    // Return user data along with session token
    stop(200, ["user" => array_merge($user, ['token' => $session['token']])]);

}, false);

/**
 * Deletes a session token (logout)
 * @example DELETE /sessions
 * @returns 204 NO CONTENT on success
 */
$app->delete('#^sessions$#', function ($params, $requestBody) {

    // Delete session
    Session::delete($this->user['token']);
    // Return 204 NO CONTENT [https://httpstatuses.com/204]
    stop(204);

});