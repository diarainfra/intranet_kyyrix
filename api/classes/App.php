<?php namespace App;


/**
 * @method get(string $string, \Closure $param, bool $needsAuthorization = true)
 * @method post(string $string, \Closure $param, bool $needsAuthorization = true)
 * @method put(string $string, \Closure $param, bool $needsAuthorization = true)
 * @method delete(string $string, \Closure $param, bool $needsAuthorization = true)
 */
class App
{
    const methods = array('GET', 'POST', 'PUT', 'DELETE');
    private $user = null;
    private $requestMethod;
    private $requestBody;

    public function __construct($method, $request_uri, $script_name)
    {

        $this->requestMethod = $method;

        // Check that we have the request uri
        if (!$request_uri) {
            throw new \Exception('$_SERVER[REQUEST_URI] is undefined. Cannot continue. Make sure .htaccess is read by Apache or that NginX is configured properly.');
        }

        // Get path from REQUEST_URI
        $path = parse_url($request_uri, PHP_URL_PATH);

        // Strip directory from $path
        define('PROJECT_DIRECTORY', dirname($script_name));
        $path = substr($path, strlen(PROJECT_DIRECTORY));


        // Split path parts into an array
        $path = explode('/', $path);


        // Remove empty values, due to leading or trailing or double slash, and renumber array from 0
        $path = array_values(array_filter($path));

        // Convert the array back to string and return it
        $this->endpoint = implode('/', $path);

        // Get request body
        $this->requestBody = file_get_contents('php://input');

        // Check that body is in JSON
        if (strlen($this->requestBody) > 0 && !$this->requestBody = json_decode($this->requestBody)) {
            stop(400, ['error' => 'Request body is not JSON']);
        }

    }

    // Catch method and call included function
    public function __call($methodFunctionName, $args)
    {
        $pattern = $args[0];
        $closure = $args[1];
        $needsAuthorization = isset($args[2]) ? $args[2] : true;

        // Uppercase method
        $methodFunctionName = strtoupper($methodFunctionName);

        if (!in_array($methodFunctionName, self::methods)) {
            throw new \Exception('Error: call to unsupported method: ' . $methodFunctionName);
        }

        if ($methodFunctionName === $this->requestMethod && preg_match($pattern, $this->endpoint, $matches)) {

            // Check auth here
            if ($needsAuthorization && !$this->user) {
                stop(401, ["error" => "Missing user token"]);
            }

            $closure($matches, $this->requestBody);
            exit();
        }
    }

    /**
     * Includes php files from given directory
     * @define "$directory" "endpoints"
     * @define "$file" "auth.php"
     **/
    public function processEndpoints($directory)
    {
        // Needed for included files
        global $app;

        $files = array_diff(scandir($directory), ['..', '.']);
        foreach ($files as $file) {
            include("$directory/$file");
        }
    }

    public function authorizeUser()
    {
        $headers = getallheaders();
        // Housekeeping
        Session::deleteExpired();
        if (!empty($headers['Authorization'])) {
            $this->user = User::get(['token'=>$headers['Authorization']]);

            if (empty($this->user)) {
                stop(401, ["error" => "Invalid user token"]);
            }

            // Make sure unprivileged user does not get forbidden data
            if (empty($this->user["isAdmin"])
                && isset($_GET['user_id'])
                && $_GET['user_id'] != $this->user['userId']) {
                stop(403, ["error" => "Forbidden"]);
            }
        }

    }

}
