import * as api from "../../node_modules/api";

export function post(req, res) {
    api.del('sessions', req.session.user && req.session.user.token).then(response => {
        delete req.session.user;
        res.end(JSON.stringify(response));
    });
}