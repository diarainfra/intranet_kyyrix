<?php namespace App;


class User
{
    public static function get($criteria)
    {
        $where = SQL::getWhere($criteria);
        return get_first("
            SELECT 
                   user_id as userId, 
                   username, 
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as name, 
                   employee_id as employeeId, 
                   eremployee.CODE as employeeCode, 
                   is_admin as isAdmin
            FROM users 
                LEFT JOIN eremployee on users.employee_id = eremployee.ID
                LEFT JOIN sessions USING (user_id)
            $where");

    }

    public static function getAll()
    {
        return get_all("
            SELECT 
                   user_id as userId, 
                   username, 
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as name, 
                   employee_id as employeeId, 
                   eremployee.CODE as employeeCode 
            FROM users
                JOIN eremployee on users.employee_id = eremployee.ID 
            WHERE RECORD_STATE != 'A'
            ORDER BY eremployee.FIRSTNAME, eremployee.SURNAME");
    }
}