<?php
/** Script to update real data with fake data
 *  for privacy and testing purposes
 */
require '../vendor/autoload.php';
require '../config.php';
require '../functions.php';
require '../database.php';

// Set the timezone to Tallinn (for DB)
date_default_timezone_set('Europe/Tallinn');

// Make a Faker and set locale to Estonia
$faker = Faker\Factory::create('et_EE');

// Delete everything older than 2016 to save space
q("DELETE bbookingrow, bbooking 
        FROM bbookingrow
        INNER JOIN bbooking ON bbookingrow.IDBBOOKING = bbooking.ID
        WHERE year(DEALDATE) < 2016");
q("DELETE FROM bbookingrow_cache WHERE YEAR < 2016 ");
q("DELETE FROM gdocument WHERE year(DEALDATE) < 2016");

// Replace employee data with fake data
$employees = get_all("SELECT  * FROM eremployee");
foreach ($employees as $employee) {
    $firstname = $faker->firstName($gender = null);
    $lastname = $faker->lastName;
    $id = substr($employee['IDENTITYCODE'], 0, 5)
        . $faker->numberBetween($min = 10, $max = 28)
        . $faker->numberBetween($min = 1000, $max = 9999);

    update('eremployee', [
        'FIRSTNAME' => $firstname,
        'SURNAME' => $lastname,
        'IDENTITYCODE' => $id,
        'STREET' => NULL,
        'PHONE' => NULL,
        'CELLULAR' => NULL,
        'CONTACTPHONE' => NULL,
        'E_MAIL' => NULL,
        'DXCODE' => NULL,
        'EXPORTCODE' => NULL
    ], "ID = $employee[ID]");
};

// Change personal project names
q("UPDATE grproject, eremployee
        SET grproject.NAME = CONCAT(eremployee.FIRSTNAME,' ',eremployee.SURNAME, ' TJ kulud')
        WHERE grproject.CODE = eremployee.CODE");

// Update usernames to new names
q("UPDATE users, eremployee
        SET users.username = LOWER(CONCAT(eremployee.FIRSTNAME,'.',eremployee.SURNAME))
        WHERE users.employee_id = eremployee.ID");

// Replace company data with fake data
$companies = get_all("SELECT * FROM grcompany");
foreach ($companies as $company) {
    $company_name = $faker->company;
    $company_street = $faker->streetAddress;
    $regno = substr($company['REGNO'], 0, 2)
        . $faker->numberBetween($min = 100000, $max = 999999);
    update('grcompany', [
        'NAME' => $company_name,
        'CODE' => $company_name,
        'LOCATIONADDR' => $company_street,
        'DELIVERYADDR' => $company_street,
        'REGNO' => $regno,
        'LEGALADDR' => $company_street,
        'PHONE' => NULL,
        'E_MAIL' => NULL,
        'DXCODE' => NULL,
        'EXPORTCODE' => NULL
    ], "ID = $company[ID]");
};

// Change project names to company names
q("UPDATE grproject, grcompany
SET grproject.NAME = grcompany.NAME
WHERE grproject.IDGRCOMPANY = grcompany.ID");

echo 'DONE';