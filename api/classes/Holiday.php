<?php namespace App;


class Holiday
{
    public static function synced(){
        $first_day_of_previous_month = date("Y-m-d", strtotime("first day of previous month"));
        return Setting::get('HOLIDAYS_LAST_SYNCED') > $first_day_of_previous_month;
    }
    public static function sync(){
        $current_year = date('Y');

        for ($year = $current_year; $year <= $current_year + 1; $year++) {

            $holidays = @file_get_contents('https://date.nager.at/Api/v2/PublicHolidays/' . $year . '/EE');

            // Validate api response
            if (!preg_match('/\[\{"date":/', $holidays, $output_array)) {
                stop(500, ['error' => 'There was an error fetching public holidays']);
            };

            $holidays = json_decode($holidays);
            foreach ($holidays as $holiday) {
                insert('intranet_holidays', [
                    'holiday_date' => $holiday->date,
                    'holiday_name' => $holiday->localName]);
            }
        }
        Setting::set('HOLIDAYS_LAST_SYNCED', date("Y-m-d"));
    }
}