<?php namespace App;


class Salary
{
    public static function projects($criteria = [], int $year)
    {
        $where = SQL::getWhere($criteria, "timesheet_id");
        $rows = get_all("
            SELECT intranet_timesheet.*,
                grproject.CODE as projectCode,
                grproject.ID as projectId,
                grproject.NAME as projectName,
                concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) as employeeName,
                eremployee.CODE AS employeeCode
            FROM grcomment 
                LEFT JOIN grproject on grproject.ID = grcomment.IDGRPROJECT
                LEFT JOIN eremployee on eremployee.ID = grcomment.IDEREMPLOYEE
                LEFT JOIN users on users.employee_id = eremployee.ID
                LEFT JOIN intranet_timesheet on grproject.ID = intranet_timesheet.project_id AND year = $year
            $where
        ");

        $result = [];
        foreach ($rows as $row) {
            $result[$row['projectId']]['projectName'] = $row['projectName'];
            $result[$row['projectId']]['projectCode'] = $row['projectCode'];
            $result[$row['projectId']]['foremanName'] = $row['employeeName'];
            $result[$row['projectId']]['foremanCode'] = $row['employeeCode'];
            if (!$row['month']) {
                $result[$row['projectId']]['months'] = [];
                continue;
            }
            $result[$row['projectId']]['months'][$row['month']] = [
                "salaryFund" => isset(self::getProjectSalaries($row['projectId'], $row['month'], $row['year'])[0]['salaryFund'])
                    ? self::getProjectSalaries($row['projectId'], $row['month'], $row['year'])[0]['salaryFund']
                    : 0,
                "salaryFundSpent" => Timesheet::salaryFundLimitReached($row['timesheet_id'], true),
                "timesheetId" => $row['timesheet_id']
            ];

        }


        return $result;
    }

    public static function employeeSalary($userId, $year)
    {
        $where = '';
        if ($userId !== "0") {
            $where = "AND users.user_id = $userId";
        }
        $rows = get_all("
                SELECT 
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) AS employeeName,
                   eremployee.CODE AS employeeCode, 
                   grproject.NAME AS projectName, 
                   grproject.CODE AS projectCode,
                   grproject.U_work_days AS workDays,
                   grproject.ID AS projectId,
                   intranet_timesheet.month,
                   intranet_timesheet.year,
                   intranet_timesheet_row.*
                FROM grcomment
                 LEFT JOIN grproject ON IDGRPROJECT = grproject.ID
                 LEFT JOIN intranet_timesheet ON IDGRPROJECT = project_id
                 LEFT JOIN intranet_timesheet_row ON intranet_timesheet.timesheet_id = intranet_timesheet_row.timesheet_id
                 LEFT JOIN eremployee ON employee_id = eremployee.ID
                 LEFT JOIN users ON IDEREMPLOYEE = users.employee_id 
                WHERE year = $year $where");

        $result = [];
        foreach ($rows as $row) {
            $result[$row['employeeCode']]['employeeName'] = $row['employeeName'];
            $result[$row['employeeCode']]['projects'][$row['projectId']]['projectName'] = $row['projectName'];
            for ($m = 1; $m <= 12; $m++) {
                if ((int)$row['month'] === $m) {
                    $result[$row['employeeCode']]['projects'][$row['projectId']]['salary'][$m] = $row['salary'];
                }
            };

        }
        $rows = get_all("
                SELECT 
                   grproject.ID AS projectId,
                   grproject.name AS projectName,
                   eremployee.CODE AS employeeCode, 
                   concat(eremployee.FIRSTNAME, ' ', eremployee.SURNAME) AS employeeName,
                   intranet_timesheet.month,
                   SUM(intranet_timesheet_additional_payments.payment) as payment
                FROM intranet_timesheet_additional_payments
                 LEFT JOIN intranet_timesheet ON intranet_timesheet.timesheet_id = intranet_timesheet_additional_payments.timesheet_id
                 LEFT JOIN grcomment ON project_id = grcomment.IDGRPROJECT
                 LEFT JOIN grproject ON IDGRPROJECT = grproject.ID
                 LEFT JOIN eremployee ON employee_id = eremployee.ID
                 LEFT JOIN users ON IDEREMPLOYEE = users.employee_id 
                WHERE year = $year $where
                GROUP BY intranet_timesheet_additional_payments.employee_id");
        foreach ($rows as $row) {
            for ($m = 1; $m <= 12; $m++) {
                $result[$row['employeeCode']]['employeeName'] = $row['employeeName'];
                $result[$row['employeeCode']]['projects'][$row['projectId']]['projectName'] = $row['projectName'];
                if ((int)$row['month'] === $m) {
                    $result[$row['employeeCode']]['projects'][$row['projectId']]['additional'][$m] = $row['payment'];
                }
            };

        }
        return $result;
    }

    public static function updateProject($data, $id)
    {
        $id = addslashes($id);
        update("grproject", (array)$data, "ID = $id");
    }

    public static function updateSalary($data)
    {
        return insert("intranet_project_salary_funds", (array)$data);

    }

    public static function deleteSalary($projectSalaryFundId)
    {
        q("DELETE FROM intranet_project_salary_funds WHERE project_salary_fund_id = $projectSalaryFundId");
    }

    public static function getProjectLastSalary($id)
    {
        return get_first("SELECT end_date as endDate FROM intranet_project_salary_funds WHERE project_id = $id ORDER BY end_date DESC");
    }

    public static function getProjectSalaries($project_id, $month, $year)
    {
        $month = $month >= 10 ? $month : "0" . $month;
        $time = $month && $year ? "AND start_date <= '$year-$month' AND end_date >= '$year-$month'" : '';
        $salaryFunds = get_all("
                    SELECT 
                            project_salary_fund_id AS projectSalaryFundId,
                            project_id AS projectId,
                            salary_fund AS salaryFund,
                            start_date as startDate,
                            end_date as endDate 
                     FROM intranet_project_salary_funds 
                     WHERE project_id = $project_id $time ");
        if (!$salaryFunds) {
            return [];
        }
        foreach ($salaryFunds as $salaryFund) {
            $result[] = [
                "salaryFund" => $salaryFund['salaryFund'],
                "projectSalaryFundId" => $salaryFund['projectSalaryFundId'],
                "startDate" => date($salaryFund['startDate']),
                "endDate" => $salaryFund['endDate']
            ];
        }
        return $result;
    }

    public static function lockSalaries($month, $year, $isLocked)
    {
        if ($isLocked) {
            q("DELETE FROM intranet_timesheet_locks WHERE month = $month and year = $year");
        } else {
            insert('intranet_timesheet_locks', ["month" => $month, "year" => $year]);
        }
    }

    public static function isLocked(int $year)
    {
        $rows = get_all("SELECT * FROM intranet_timesheet_locks WHERE year = $year");
        $result = [];
        for ($i = 1; $i <= 12; $i++) {
            $result[$i] = false;
            foreach ($rows as $row) {
                if ((int)$row['month'] === $i) {
                    $result[$row['month']] = true;
                }
            }
        }
        return $result;

    }

    public static function getLocked($year, $month)
    {
        return get_all("SELECT * FROM intranet_timesheet_locks WHERE year = $year AND month = $month");
    }
}