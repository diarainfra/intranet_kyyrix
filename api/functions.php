<?php


function handleCors()
{
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    header("Access-Control-Allow-Headers: *");
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        exit();
    }
}

function stop($code, $data = null)
{
    $response['status'] = $code;

    http_response_code($code);

    // Echo JSON if there is data
    echo $data ? json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) : '';

    exit();
}


function print_var($var)
{
    if (is_string($var))
        return ('"' . str_replace(array("\x00", "\x0a", "\x0d", "\x1a", "\x09"), array('\0', '\n', '\r', '\Z', '\t'), $var) . '"');
    else if (is_bool($var)) {
        if ($var)
            return ('true');
        else
            return ('false');
    } else if (is_array($var)) {
        $result = 'array( ';
        $comma = '';
        foreach ($var as $key => $val) {
            $result .= $comma . print_var($key) . ' => ' . print_var($val);
            $comma = ', ';
        }
        $result .= ' )';
        return ($result);
    }

    return (var_export($var, true));    // anything else, just let php try to print it
}

function trace($levels_to_omit)
{
    $output = "";
    $trace = debug_backtrace();

    // Remove topmost function calls
    for ($level = $levels_to_omit; $level > 0; $level--) {
        array_shift($trace);
    }


    foreach ($trace as $val) {

        $level++;


        if ($val['function'] == 'include' ||
            $val['function'] == 'require' ||
            $val['function'] == 'include_once' ||
            $val['function'] == 'require_once')
            $func = '';
        else {
            $func = $val['function'] . '(';

            if (isset($val['args'][0])) {
                $func .= ' ';
                $comma = '';
                foreach ($val['args'] as $val2) {
                    $func .= $comma . print_var($val2);
                    $comma = ', ';
                }
                $func .= ' ';
            }

            $func .= ')';
        }

        $output .= $val['file'] . ':' . $val['line'];

        if ($func) $output .= ' ' . $func;

        $output .= "; ";

    }

    // Remove root directory from output
    $root_dir = __DIR__;
    $output = str_replace($root_dir, '', $output);

    return trim("$output");
}

function isJson(string $string) {
    return ($result = json_decode($string, true)) ? true : false;
}