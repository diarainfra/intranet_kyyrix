import {
    register,
    init,
} from 'svelte-i18n';

const INIT_OPTIONS = {
    fallbackLocale: 'et',
    initialLocale: 'et',
    loadingDelay: 200,
    formats: {},
    warnOnMissingMessages: true,
};

register('et', () => import('dictionaries/et.json'));

// initialize the i18n library in client
export function startClient() {
    init({
        ...INIT_OPTIONS
    });
}

// initialize the i18n library in the server and returns its middleware
export function i18nMiddleware() {

    // initialLocale will be set by the middleware
    init(INIT_OPTIONS);

    return (req, res, next) => {
        next();
    };
}
