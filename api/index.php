<?php use App\App;

include 'config.php';

// Show all errors for easier debugging
if (!PRODUCTION) {

    //Disable showing ugly HTML on error conditions
    if (function_exists('xdebug_disable')) {
        xdebug_disable();
    }

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
}

ob_start();

include 'functions.php';
include 'database.php';
include 'vendor/autoload.php';

handleCors();

try {

    $app = new App(
        $_SERVER['REQUEST_METHOD'],
        $_SERVER['REQUEST_URI'],
        $_SERVER['SCRIPT_NAME']
    );

} catch (Exception $e) {
    var_dump($e);
}

$app->authorizeUser();
$app->processEndpoints('endpoints/');

http_response_code(404);